import 'package:flutter/material.dart';

import 'advice.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String adviceText =
      "Some of life's best lessons are learnt at the worst times.";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 50.0,
            margin: const EdgeInsets.fromLTRB(30, 15, 30, 60),
            child: Container(
              height: 44.0,
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(5.0)),
              child: const Text(
                'Byte Advice',
                style: TextStyle(fontSize: 40, color: Colors.blueGrey),
              ),
            ),
          ),
          Container(
            width: 300,
            height: 300,
            decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                boxShadow: const [
                  BoxShadow(
                      color: Colors.grey,
                      offset: Offset(5.0, 5.0),
                      blurRadius: 15.0,
                      spreadRadius: 1.0),
                  BoxShadow(
                      color: Colors.white,
                      offset: Offset(-5.0, -5.0),
                      blurRadius: 15.0,
                      spreadRadius: 1.0),
                ]),
            child: Padding(
              padding: const EdgeInsets.all(30.0),

                child: Text(
                  adviceText,
                  overflow: TextOverflow.visible,
                  textAlign: TextAlign.start,
                  style: const TextStyle(fontSize: 30.0),
                ),
            
            ),
          ),
          Container(
            height: 50.0,
            margin: const EdgeInsets.all(30),
            child: Container(
              height: 44.0,
              decoration: BoxDecoration(
                  gradient: const LinearGradient(
                    colors: [Color(0xff374ABE), Color(0xff64B6FF)],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                  borderRadius: BorderRadius.circular(5.0)),
              child: ElevatedButton(
                onPressed: () async => await setAdviceText(),
                style: ElevatedButton.styleFrom(
                    primary: Colors.transparent,
                    shadowColor: Colors.transparent),
                child: const Text('Get Advice'),
              ),
            ),
          ),
        ],
      )),
    );
  }

  setAdviceText() async {
    String text = await getAdviceText();
    setState(() {
      adviceText = text;
    });
  }
}
