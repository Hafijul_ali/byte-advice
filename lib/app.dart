import 'package:flutter/material.dart';
import 'homepage.dart';

const appTitle = 'Byte Advice';
class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: const HomePage(title: appTitle),
    );
  }
}

