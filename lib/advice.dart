import 'dart:convert';

import 'package:http/http.dart' as http;

Future<String> getAdviceText() async {
  Uri endpoint = Uri.parse('https://api.adviceslip.com/advice');
  String? advice = "";
  final response = await http.get(endpoint);
  Map<String, dynamic> responseData = jsonDecode(response.body);
  Map<String, dynamic>? slip = responseData['slip'];
  advice = slip!['advice'];
  if(advice != null) {
    return advice;
  }
  return "No Advice at this time";
}
